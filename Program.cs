﻿using System;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            Node rootNode = new Node { Id = 10 };
            rootNode.AddChild(5);
            rootNode.AddChild(1);
            rootNode.AddChild(20);
            rootNode.AddChild(10);

            var aNode = rootNode.AddChild(7);

            //aNode.Left = new Node { Id = 4 };

            var isValid = rootNode.VerifyTree();

            Console.WriteLine("Tree is {0}", isValid ? "Valid" : "Invalid");
            Console.ReadKey();
        }
    }
    public static class NodeFunctions
    {
        public static Node AddChild(this Node rootNode, int id)
        {
            if (id > rootNode.Id)
            {
                if (rootNode.Right == null)
                {
                    rootNode.Right = new Node { Id = id };
                    return rootNode.Right;
                }
                else
                {
                    return rootNode.Right.AddChild(id);
                }
            }
            else
            {
                //id<=Node.Id
                if (rootNode.Left == null)
                {
                    rootNode.Left = new Node { Id = id };
                    return rootNode.Left;
                }
                else
                {
                    return rootNode.Left.AddChild(id);
                }
            }
        }

        public static bool VerifyTree(this Node rootNode)
        {
            return VerifyTree(rootNode, int.MaxValue, int.MinValue);
        }

        private static bool VerifyTree(Node node, int branchMaxId, int branchMinId)
        {
            bool result = node.Id <= branchMaxId && node.Id > branchMinId;

            if (result && node.Right != null)
            {
                result = VerifyTree(node.Right, branchMaxId, node.Id);
            }

            if (result && node.Left != null)
            {
                result = VerifyTree(node.Left, node.Id, branchMinId);
            }

            return result;
        }
    }

    public class Node
    {
        public Node Left { get; set; }
        public Node Right { get; set; }
        public int Id { get; set; }
    }
}
